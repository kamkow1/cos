# cOS (something OS)
A simple OS written in Rust. Doesn't solve anything, but is a good educational tool (for me ;) )

# Building

requirements: cargo, qemu-system-x86\_64, qemu-img

```console
make build
```
To clean run:
```console
make clean
```

# Running

```console
make run
```

For development:
```console
make dev
```
This target will build all components, format the disk, setup the filesystem catalogs
and run the image in Qemu.

# Testing

To test the filesystem:
```console
make test_fsys
```

# Credits
- [Philipp's github page](https://github.com/phil-opp)
- [Blog OS](https://github.com/phil-opp/blog_os)
- [Moros (a huge inspiration)](https://github.com/vinc/moros)
- [Thesus OS](https://github.com/theseus-os/Theseus)
