#![no_std]
#![no_main]

extern crate sys_api;
extern crate alloc;

mod ui;

use spin::Mutex;
use core::str;
use alloc::alloc::{alloc, dealloc, Layout};
use core::alloc::GlobalAlloc;
use alloc::vec::Vec;
use alloc::boxed::Box;
use alloc::string::String;
use alloc::format;
use sys_api::{syscall, syscall::*};
use sys_api::fsys2;
use sys_api::proc;
use sys_api::fsys::inode::INode;
use crate::ui::PROMPT;

fn display_self_out(outfd: u32) {
    let layout = Layout::new::<INode>();
    let ptr = unsafe { alloc(layout) as *mut INode };
    let inode = unsafe { Box::from_raw(ptr) };
    unsafe { syscall!(FD_INFO, outfd, ptr) };
    for i in 0..(*inode).size {
        let chr = fsys2::read_byte(outfd, i as usize);
        unsafe { syscall!(VGA_PRINT, chr) };
    }

    unsafe { syscall!(VGA_PRINT, '\n') };
}

static SHELL_OPTIONS: Mutex<ShellOptions> = Mutex::new(ShellOptions::default());

struct ShellOptions {
    master: bool, // if true, the shell shouldn't exit
}

impl ShellOptions {
    const fn default() -> Self {
        Self {
            master: false,
        }
    }
}

#[no_mangle]
pub extern fn main(options: &String) {
    let outfd = proc::out_handle();
    fsys2::append_bytes(outfd, PROMPT.as_bytes());
    fsys2::append_bytes(outfd, b"\n");

    if SHELL_OPTIONS.lock().master {
        display_self_out(outfd as u32);
        loop {}
    }
    sys_api::proc::exit();
}

