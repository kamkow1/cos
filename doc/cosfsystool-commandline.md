# cosfsystool commandline options
This file documents the commandline options for manual invocation of `cosfsystool`

## Abbreviations
| abbreviation | meaning  |
|:------------:|:--------:|
| B            | Bootable |
| C            | Catalog  |
| F            | File     |

## Bootable or Non-bootable
When invoking the tool you need to specify whether it's interacting with a bootable
drive or not. A bootable drive will contain the kernel code, so the actual filesystem
starts at a certain offset. This is important, because we don't want the tool to
overwrite the kernel code.

This is how the tool will be invoked:

For working with a bootable drive
```console
cosfsystool B <other options>
```

For working with non-bootable drives
```console
cosfsystool _ <other options>
```

`B` means "bootable", but in the example above we specify `_` as the drive type.
This character can be anything - it's not limited to just `_`. The tool will treat
the drive as bootable only if the character is `B`, anything else means non-bootable.

## Formatting

```console
cosfsystool B fmt <drive path>
```

Invoking this command will result in formatting the drive. Now the drive contains
the Superblock and the root catalog (`Root-Catalog`)

## Writting

Abstract:
```console
cosfsystool B write <drive path> <INode kind> <path> <file (from host OS)>
```

Real example (taken from the Makefile):
```console
cosfsystool B write <drive> C 'Root-Catalog\Test3'
cosfsystool B write <drive> F 'Root-Catalog\Test\file.text' TEST1
```

### `<path>`
All paths MUST start with `Root-Catalog`. As of now, the filesystem doesn't default to
using `Root-Catalog` as the default parent catalog.

### `<INode kind>`
`C` for creating catalogs or `F` for creating files. When creating a file, the user
must provide the input file from the host OS's filesystem.

## Reading

```console
cosfsystool B read <drive> <path>
```

## Removing

```console
cosfsystool B remove <drive> <path>
```

### `<path>`
The path can be anything - a file or a catalog.

## Listing INodes - useful for debugging

```console
cosfsystool B list-inodes <drive>
```

This command will output all of the inodes stored on the filesystem.

## Tree

```console
cosfsystool B tree <drive> <path>
```

