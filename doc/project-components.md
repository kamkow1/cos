# Components

## kernel
Contains the code of the kernel

## fsys
COS filesystem library. Used by the kernel and fsystool

## fsystool
A tool that uses the fsys library to create and modify the filesystem. Useful for testing.

## sys\_api 
Cosapi64 is a 64 bit API for interacting with the OS. Use this library to develop user-space programs.

## gen\_alloc
A library with generic `#[no_std]` allocators. Used both in the kernel and in `sys_api`.
