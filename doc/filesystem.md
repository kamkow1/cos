# COS Filesystem

## Architecture
The entire filesystem is stored in-memory (for simplicity). It can be later
serialized into bytes for writing / synchronizing with the actual backend.

The filesystem itself is a library that doesn't deal with IO itself - it needs
a backend for that. Here's a graphical explanation:

```text
+-----------------------------------------------------------------------------+
|                                                                             |
|   Filesystem Library (Abstract filesystem structures and utility functions) |
|       | |                                                                   |
|       | |                                                                   |
|       | |                                                                   |
|       +-+---------------------> cosfsystool (runs on the host OS)           |
|         |                                                                   |
|         |                       Interacts with the disk image as a          |
|         |                       regular file. Performs IO operations.       |
|         |                                                                   |
|         |                                                                   |
|         |                                                                   |
|         +---------------------> kernel (runs on the hardware)               |
|                                                                             |
|                                 Interacts with the disk as an external      |
|                                 device. Performs IO operations.             |
|                                                                             |
+-----------------------------------------------------------------------------+
```

## Components
This section explains the filesystem components - the superblock, inodes and blocks.

```text
+-------------------------------------------------------------------------------+
|                                                                               |
|  Filesystem                                                                   |
|   | | |                                                                       |
|   +-+-+--------------------> SuperBlock (abstract)                            |
|     | |                      |                                                |
|     | |                      +-----> PhysicalSuperBlock (serializable)        |
|     | |                                                                       |
|     | |                              +---------------+                        |
|     | |                              | - signature   |                        |
|     | |                              | - inode_count |                        |
|     | |                              | - block_count |                        |
|     | |                              +---------------+                        |
|     | |                                                                       |
|     +-+--------------------> INode (serializable)                             |
|       |                                                                       |
|       |                     +---------------------------------------------+   |
|       |                     | - name                                      |   |
|       |                     | - first_block (beginning of the block chain)|   |
|       |                     | - size (size of bytes stored)               |   |
|       |                     | - children (for catalogs)                   |   |
|       |                     | - kind (File or Catalog)                    |   |
|       |                     +---------------------------------------------+   |
|       |                                                                       |
|       +-------------------> Block (serializable)                              |
|                                                                               |
|                             +-----------------------------------------------+ |
|                             |- next_block (index of the next block in chain)| |
|                             |- bytes (block contetns)                       | |
|                             +-----------------------------------------------+ |
|                                                                               |
+-------------------------------------------------------------------------------+
```

Here, the blocks resemble how the FAT filesystem is implemented. An INode points to a chain of blocks
and a block points to the next one in the chain.
