#![no_std]
#![feature(const_mut_refs)]

extern crate alloc;

pub mod bump;
pub mod linked_list;
pub mod fixed_size_block;

/// A wrapper around spin::Mutex to permit trait implementations.
pub struct Locked<A> {
    inner: spin::Mutex<A>,
}

impl<A> Locked<A> {
    pub const fn new(inner: A) -> Self {
        Locked { inner: spin::Mutex::new(inner) }
    }

    pub fn lock(&self) -> spin::MutexGuard<A> {
        self.inner.lock()
    }
}

/// Align the given address `addr` upwards to alignment `align`.
///
/// Requires that `align` is a power of two.
pub fn align_up(addr: usize, align: usize) -> usize {
    (addr + align - 1) & !(align - 1)
}

pub fn nearest_pow_of_2(x: usize) -> usize {
    let mut k = 1usize;
    while (k < x) {
        k *= 2;
    }
    k
}
