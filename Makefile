FSYS_TOOL = ./fsystool/target/debug/fsystool
DISK = kernel/disk.img

.PHONY: build
build: build_fsystool build_kernel build_programs


.PHONY: build_programs
build_programs:
	cd programs; \
	make; \
	make build_rust;

.PHONY: build_fsystool
build_fsystool:
	cd fsystool; \
	cargo build;

.PHONY: build_kernel
build_kernel:
	cd kernel; \
	make;

.PHONY: run
run:
	cd kernel; \
	make run;

.PHONY: clean
clean: clean_kernel clean_fsystool clean_programs

.PHONY: clean_programs
clean_programs:
	cd programs; \
	make clean;

.PHONY: clean_fsystool
clean_fsystool:
	cd fsystool; \
	cargo clean;

.PHONY: clean_kernel
clean_kernel:
	cd kernel; \
	make clean;

.PHONY: fmt
fmt:
	$(FSYS_TOOL) B fmt $(DISK)

.PHONY: test_fsys
test_fsys:
	touch TEST1 && echo "TEST1 file contents" > TEST1
	touch TEST2 && echo "TEST2 file contents" > TEST2
	touch LARGE_FILE && dd if=/dev/urandom bs=512 count=2 | base64 > LARGE_FILE

	-$(FSYS_TOOL) B fmt $(DISK)
	
	-$(FSYS_TOOL) B write $(DISK) C 'Root-Catalog\Test'
	-$(FSYS_TOOL) B write $(DISK) C 'Root-Catalog\Test2'
	-$(FSYS_TOOL) B write $(DISK) C 'Root-Catalog\Test3'

	-$(FSYS_TOOL) B write $(DISK) F 'Root-Catalog\Test\file.text' TEST1
	-$(FSYS_TOOL) B write $(DISK) F 'Root-Catalog\Test2\file.text' TEST2
	-$(FSYS_TOOL) B write $(DISK) F 'Root-Catalog\Test3\file.text' LARGE_FILE

	-$(FSYS_TOOL) B read $(DISK) 'Root-Catalog\Test\file.text'
	-$(FSYS_TOOL) B read $(DISK) 'Root-Catalog\Test2\file.text'
	-$(FSYS_TOOL) B read $(DISK) 'Root-Catalog\Test3\file.text'

	-$(FSYS_TOOL) B remove $(DISK) F 'Root-Catalog\Test\file.text'
	-$(FSYS_TOOL) B remove $(DISK) C 'Root-Catalog\Test'
	-$(FSYS_TOOL) B remove $(DISK) C 'Root-Catalog\Test2'
	-$(FSYS_TOOL) B remove $(DISK) C 'Root-Catalog\Test3'
	
	-$(FSYS_TOOL) B read $(DISK) 'Root-Catalog\Test\file.text' > /dev/null 2>&1 
	-$(FSYS_TOOL) B read $(DISK) 'Root-Catalog\Test2\file.text' > /dev/null 2>&1
	-$(FSYS_TOOL) B read $(DISK) 'Root-Catalog\Test3\file.text' > /dev/null 2>&1

	-$(FSYS_TOOL) B list-inodes $(DISK)

	rm -f TEST1 TEST2 LARGE_FILE

.PHONY: programs
programs:
	@for file in programs/*.o; \
	do \
		file=$$(basename $$file); \
		echo "file = $$file"; \
		new_file="$${file%.o}.app"; \
		echo "app = $$new_file"; \
		$(FSYS_TOOL) \
			B write $(DISK) \
			F "Root-Catalog\Programs\\$$new_file" programs/$$file; \
		echo "wrote $$new_file"; \
	done

# NOTE: 20 is a very machine dependant value
# on my machine, 20 is just enough for the OS to boot up and do it's thing.
# On slower setups 20 probably won't last long enough
.PHONY: fsys_structure
fsys_structure:
	$(FSYS_TOOL) B structure $(DISK)

.PHONY: dev
dev: | build fmt fsys_structure programs run

