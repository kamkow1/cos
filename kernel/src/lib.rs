#![no_std]
#![cfg_attr(test, no_main)]
#![feature(custom_test_frameworks, naked_functions, abi_x86_interrupt, const_mut_refs, allocator_api)]
#![test_runner(crate::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;
extern crate fsys;
extern crate sys_api;
extern crate gen_alloc;

use core::panic::PanicInfo;
use bootloader::BootInfo;

pub mod allocator;
pub mod gdt;
pub mod interrupts;
pub mod memory;
pub mod serial;
pub mod task;
pub mod vga_buffer;
pub mod ata;
pub mod fs; 
pub mod logging;
pub mod proc;
pub mod syscall;

pub fn init(boot_info: &'static BootInfo) {
    use x86_64::VirtAddr;

    gdt::init();
    interrupts::init_idt();
    unsafe { interrupts::PICS.lock().initialize() };
    x86_64::instructions::interrupts::enable();

    unsafe { memory::PHYSICAL_MEM_OFFSET = boot_info.physical_memory_offset };
    unsafe { memory::MEM_MAP.replace(&boot_info.memory_map) };

    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    unsafe {
        let mut mapper = memory::init(phys_mem_offset);
        let mut frame_allocator = memory::BootInfoFrameAllocator::init(&boot_info.memory_map);
        allocator::init_heap(&mut mapper, &mut frame_allocator)
            .expect("heap initialization failed");
    }

    ata::init();
    for (i, drive) in ata::DRIVES.lock().iter().enumerate() {
        fs::init(&drive, fsys::Options { boot_drive: i == 0 });
    }
    logging::init();
}

pub trait Testable {
    fn run(&self) -> ();
}

impl<T> Testable for T
where
    T: Fn(),
{
    fn run(&self) {
        serial_print!("{}...\t", core::any::type_name::<T>());
        self();
        serial_println!("[ok]");
    }
}

pub fn test_runner(tests: &[&dyn Testable]) {
    serial_println!("Running {} tests", tests.len());
    for test in tests {
        test.run();
    }
    exit_qemu(QemuExitCode::Success);
}

pub fn test_panic_handler(info: &PanicInfo) -> ! {
    serial_println!("[failed]\n");
    serial_println!("Error: {}\n", info);
    exit_qemu(QemuExitCode::Failed);
    hlt_loop();
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum QemuExitCode {
    Success = 0x10,
    Failed = 0x11,
}

pub fn exit_qemu(exit_code: QemuExitCode) {
    use x86_64::instructions::port::Port;

    unsafe {
        let mut port = Port::new(0xf4);
        port.write(exit_code as u32);
    }
}

pub fn hlt_loop() -> ! {
    loop {
        x86_64::instructions::hlt();
    }
}

#[cfg(test)]
use bootloader::{entry_point, BootInfo};

#[cfg(test)]
entry_point!(test_kernel_main);

/// Entry point for `cargo xtest`
#[cfg(test)]
fn test_kernel_main(_boot_info: &'static BootInfo) -> ! {
    init();
    test_main();
    hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    test_panic_handler(info)
}
