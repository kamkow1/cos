use alloc::string::{String, ToString};
use alloc::format;
use lazy_static::lazy_static;
use spin::Mutex;
use crate::fs;

pub const LOG_FILE_PATH: &str = "Root-Catalog\\System\\Logs\\logs.text";

lazy_static! {
    pub static ref LOGS_ENABLED: Mutex<bool> = Mutex::new(false);
}

pub fn log(s: &str) {
    let fd = fs::open(LOG_FILE_PATH).unwrap();
    let contents = fs::read_bytes(fd);
    let new_contents = [&contents, s.as_bytes()].concat();
    fs::write_bytes(fd, &new_contents).unwrap();
}

pub fn log_file() -> String {
    let fd = fs::open(LOG_FILE_PATH).unwrap();
    let contents = fs::read_bytes(fd);
    String::from_utf8_lossy(&contents).to_string()
}

pub fn init() {
    *LOGS_ENABLED.lock() = true;
    log("\nLogging initialized\n");
    log(format!("Log file: {}\n", LOG_FILE_PATH).as_str());
    crate::println!("{}", log_file());
}
