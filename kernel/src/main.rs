#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(cos::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use cos::println;
use cos::ata;
use cos::fs;
use cos::proc;
use cos::task::{self, executor::Executor, keyboard, Task};
use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use core::str;
use alloc::string::String;

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {
    println!("Hello World{}", "!");

    cos::init(boot_info);

    #[cfg(test)]
    test_main();

    let mut executor = Executor::new();
    // executor.spawn(Task::new(keyboard::print_keypresses()));
    for (i, drive) in ata::DRIVES.lock().iter().enumerate() {
        let options = fsys::Options { boot_drive: i == 0 };
        executor.spawn(Task::new(task::fsys::sync_drive_periodically(drive.clone(), options)));
    }

    // instantiate startup process and run
    let startup_file = fs::open("Root-Catalog\\System\\startup.text").unwrap();
    let contents = fs::read_bytes(startup_file);

    let path = str::from_utf8(&contents).unwrap();
    let program_file = fs::open(&path).unwrap();
    let program_bytes = fs::read_bytes(program_file);
    println!("Running startup program: {}\n", &path);
    proc::spawn(&program_bytes, String::from("master false")); // display output to the vga buffer

    executor.run();
}

/// This function is called on panic.
#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    cos::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    cos::test_panic_handler(info)
}

#[test_case]
fn trivial_assertion() {
    assert_eq!(1, 1);
}
