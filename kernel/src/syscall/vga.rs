// vga buffer related syscalls
use alloc::vec;
use core::str;

pub fn vga_print(data: u8) -> usize {
    crate::print!("{}", data as char);
    0usize
}
