use alloc::alloc::Layout;
use sys_api::syscall::*;
use crate::fsys::inode::INode;

pub mod fsys;
pub mod memory;
pub mod proc;
pub mod vga;

#[allow(dead_code)]
pub fn dispatch(n: usize, arg1: usize, arg2: usize, arg3: usize, arg4: usize) -> usize {
    // crate::logging::log(format!("Syscall: {}\n", n).as_str());
    match n {
        // ---- Process ----
        EXIT         => proc::exit(),
        HALT         => proc::halt(),
        OUT_HANDLE   => proc::out_handle(),
        IN_HANDLE    => proc::in_handle(),
        // ptr to core::panic:PanicInfo
        PANIC_NOTIFY => proc::panic_notify(arg1 as usize),

        // ---- Filesystem ----
        // fd, position, byte
        WRITE        => fsys::write(arg1 as u32, arg2 as i64, arg3 as u8),
        // fd, position
        READ         => fsys::read(arg1 as u32, arg2 as i64),
        // fd, ptr to preallocated fsys::inode::INode
        FD_INFO      => fsys::fd_info(arg1, arg2 as *mut INode),
        // ptr to string path, length
        OPEN         => fsys::open(arg1 as *mut u8, arg2),
        // fd, byte
        APPEND       => fsys::append(arg1 as u32, arg2 as u8),

        // ---- Memory ----
        // ptr to alloc::alloc::Layout
        MEM_ALLOC    => memory::mem_alloc(arg1 as *const Layout),
        // ptr, ptr to alloc::alloc::Layout
        MEM_DEALLOC  => memory::mem_dealloc(arg1 as *mut u8, arg2 as *const Layout),
       
        // ---- Vga ----
        // character
        VGA_PRINT    => vga::vga_print(arg1 as u8),
        _            => unimplemented!("n = {:#X}", n),
    }
}
