// process related syscalls
use crate::proc;
use crate::logging;

pub fn exit() -> usize {
    proc::exit();
    0usize
}

pub fn halt() -> usize {
    logging::log("Halting...\n");
    unsafe { core::arch::asm!("hlt") };
    0usize
}

pub fn out_handle() -> usize {
    proc::get_out().fd as usize
}

pub fn in_handle() -> usize {
    proc::get_in().fd as usize
}

pub fn panic_notify(ptr: usize) -> usize {
    use core::panic::PanicInfo;

    let info_ptr = ptr as *const PanicInfo;
    let info = unsafe { &*info_ptr };
    crate::println!("App Panicked!");

    // proc::exit();
    0usize
}
