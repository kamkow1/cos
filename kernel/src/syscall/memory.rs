// memory management related syscalls
use crate::allocator;
use alloc::alloc::{GlobalAlloc, Layout};

pub fn mem_alloc(layout: *const Layout) -> usize {
    let x = unsafe { allocator::USER_ALLOCATOR.alloc(*layout) as usize };
    x
}

pub fn mem_dealloc(ptr: *mut u8, layout: *const Layout) -> usize {
    unsafe { allocator::USER_ALLOCATOR.dealloc(ptr, *layout) };
    0usize
}
