// filesystem related syscalls
use core::str;
use alloc::string::String;
use crate::fs;
use crate::fsys::inode::INode;

pub fn write(fd: u32, position: i64, byte: u8) -> usize {
    fs::write_byte(fd, position, byte).unwrap();
    position as usize
}

pub fn read(fd: u32, position: i64) -> usize {
    fs::read_byte(fd, position) as usize
}


pub fn append(fd: u32, byte: u8) -> usize {
    let mut bytes = fs::read_bytes(fd);
    bytes.push(byte);
    fs::write_bytes(fd, &bytes).unwrap();
    bytes.len()
}

pub fn fd_info(fd: usize, ptr: *mut INode) -> usize {
    let inode = fs::FILESYSTEM.lock().as_ref().unwrap().inodes[fd];
    unsafe { core::ptr::write(ptr, inode) };
    ptr as usize
}

pub fn open(bytes: *mut u8, len: usize) -> usize {
    let slice = unsafe { core::slice::from_raw_parts(bytes, len) };
    let string = String::from_utf8_lossy(slice);
    let path = fsys::make_path(&string);
    let inode = fs::FILESYSTEM.lock().as_ref().unwrap().fd(&path);
    if inode.is_err() {
        usize::MAX
    } else {
        inode.unwrap() as usize
    }
}

