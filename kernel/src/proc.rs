use core::sync::atomic::{AtomicUsize, Ordering};
use core::fmt;
use alloc::alloc::{alloc, dealloc, Layout, GlobalAlloc, Global};
use alloc::string::String;
use alloc::vec::Vec;
use alloc::format;
use x86_64::structures::idt::InterruptStackFrameValue;
use x86_64::VirtAddr;
use lazy_static::lazy_static;
use spin::Mutex;
use object::{Object, ObjectSegment};
use crate::allocator;
use crate::gdt::GDT;
use crate::fs;
use fsys::inode::INodeKind;

const USERSPACE_START: usize = allocator::USER_HEAP_START;
const MAX_PROCS: usize = 2;

lazy_static! {
    pub static ref PROC_BASE_ADDR: AtomicUsize = AtomicUsize::new(USERSPACE_START);
    pub static ref PROC_COUNT: Mutex<u32> = Mutex::new(1u32);
    pub static ref PROCS: Mutex<[Proc; MAX_PROCS]> = Mutex::new([(); MAX_PROCS].map(|_| Proc::new(0, 0, 0, 0, 0)));
}

static PROC_COUNTER: AtomicUsize = AtomicUsize::new(0);

#[derive(Default, Copy, Clone)]
pub struct Registers {
    pub r11: usize,
    pub r10: usize,
    pub r9: usize,
    pub r8: usize,
    pub rdi: usize,
    pub rsi: usize,
    pub rdx: usize,
    pub rcx: usize,
    pub rax: usize,
}

pub const MAX_HANDLES: usize = 2;

#[derive(Default, Copy, Clone, Debug)]
pub struct Handle {
    pub fd: u32,
}

#[derive(Copy, Clone)]
pub struct Proc {
    pub id: usize,
    pub alloc_addr: u64,
    pub alloc_layout: Option<Layout>,
    pub stack_addr: u64,
    pub start_addr: u64,
    pub args_addr: u64,
    pub args_layout: Option<Layout>,
    pub args_len: usize,
    pub size: usize,
    pub registers: Registers,
    pub stack_frame: InterruptStackFrameValue,
    // 0 -> in
    // 1 -> out
    pub handles: [Handle; MAX_HANDLES],
}

impl Proc {
    pub fn new(id: usize, alloc_addr: u64, stack_addr: u64, start_addr: u64, size: usize) -> Self {
        let stack_frame = InterruptStackFrameValue {
            instruction_pointer: VirtAddr::new(0),
            code_segment: 0,
            cpu_flags: 0,
            stack_pointer: VirtAddr::new(0),
            stack_segment: 0,
        };
        let mut handles = [Handle::default(); MAX_HANDLES];
        // non-default Procs will have id >= 1, so we use 0 to invalidate default Procs
        // default Proc structures are uninstantiated processes
        // they don't need any physical handles
        if id != 0 { 
            // crate the process specific catalog
            let fd = fs::create(alloc::format!("Root-Catalog\\System\\Process\\{}", id).as_str(), INodeKind::Catalog).unwrap();
            // and the handles
            handles[0] = Handle { fd: fs::create(alloc::format!("Root-Catalog\\System\\Process\\{}\\in", id).as_str(),    INodeKind::File).unwrap() };
            handles[1] = Handle { fd: fs::create(alloc::format!("Root-Catalog\\System\\Process\\{}\\out", id).as_str(),   INodeKind::File).unwrap() };
        }
        Self {
            id,
            alloc_addr,
            alloc_layout: None,
            stack_addr,
            start_addr,
            args_addr: 0,
            args_layout: None,
            args_len: 0,
            size,
            registers: Registers::default(),
            stack_frame,
            handles,
        }
    }
}

impl fmt::Debug for Proc {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Proc")
            .field("id", &self.id)
            .field("alloc_addr", &core::format_args!("{:#X}", &self.alloc_addr))
            .field("stack_addr", &core::format_args!("{:#X}", &self.stack_addr))
            .field("start_addr", &core::format_args!("{:#X}", &self.start_addr))
            .field("args_addr",  &core::format_args!("{:#X}", &self.args_addr))
            .field("args_len",   &self.args_len)
            .field("size",       &self.size)
            .finish()
    }
}

pub fn get_handle(h: usize) -> Handle {
    let proc = &PROCS.lock()[PROC_COUNTER.load(Ordering::SeqCst) - 1];
    proc.handles[h]
}

pub fn get_in() -> Handle {
    get_handle(0)
}

pub fn get_out() -> Handle {
    get_handle(1)
}

pub fn exit() {
    let proc = &PROCS.lock()[PROC_COUNTER.load(Ordering::SeqCst) - 1];

    // remove handles
    fs::remove(alloc::format!("Root-Catalog\\System\\Process\\{}\\in", proc.id).as_str(), INodeKind::File).unwrap();
    fs::remove(alloc::format!("Root-Catalog\\System\\Process\\{}\\out", proc.id).as_str(), INodeKind::File).unwrap();
    // dealloc memory
    unsafe { allocator::USER_ALLOCATOR.dealloc(proc.args_addr as *mut u8, proc.args_layout.unwrap()) };
    unsafe { allocator::USER_ALLOCATOR.dealloc(proc.alloc_addr as *mut u8, proc.alloc_layout.unwrap()) };

    PROC_COUNTER.fetch_sub(1, Ordering::SeqCst);
}
    
pub fn spawn(bytes: &[u8], arguments: String) {
    let size: usize = 1 << 20; // 1MB

    let mut id = PROC_COUNTER.fetch_add(1, Ordering::SeqCst);
    id += 1;
    let mut proc = Proc::new(id, 0u64, 0u64, 0, size);
    
    // Code
    {
        let layout = Layout::from_size_align(size, 1024*1024).unwrap();
        let code_ptr = unsafe { allocator::USER_ALLOCATOR.alloc(layout) };
        const ELF_MAGIC: [u8; 4] = [0x7F, b'E', b'L', b'F'];
        if bytes[0..4] == ELF_MAGIC {
            let o = object::File::parse(bytes).unwrap();
            proc.start_addr = o.entry();
            for segment in o.segments() {
                let addr = segment.address() as usize;
                if let Ok(data) = segment.data() {
                    for (i, b) in data.iter().enumerate() {
                        unsafe { core::ptr::write(code_ptr.add(i + addr), *b) };
                    }
                }
            }
        }
        proc.alloc_addr = code_ptr as u64;
        proc.alloc_layout = Some(layout);
    }

    // Stack
    {
        let stack_size: usize = 0x1000;
        let layout = Layout::from_size_align(size, size).unwrap();
        let stack_alloc_addr = unsafe { allocator::USER_ALLOCATOR.alloc(layout) };
        // + stack_size, because the stack grows downwards
        proc.stack_addr = stack_alloc_addr as u64 + stack_size as u64;
    }
        
    // Arguments
    {
        assert!(arguments.len() <= 1024); // handle only 1kib of arguments
        let layout = Layout::from_size_align(arguments.len(), 1024).unwrap();
        let args_ptr = unsafe { allocator::USER_ALLOCATOR.alloc(layout) };
        for (i, b) in arguments.as_str().as_bytes().into_iter().enumerate() {
            unsafe { core::ptr::write(args_ptr.offset(i as isize), *b) };
        }

        proc.args_addr = args_ptr as u64;
        proc.args_layout = Some(layout);
        proc.args_len = arguments.len();
    }
    
    PROCS.lock()[PROC_COUNTER.load(Ordering::SeqCst) - 1] = proc;

    x86_64::instructions::tlb::flush_all();
    unsafe {
        core::arch::asm!(
            "cli",
            "push {:r}",
            "push {:r}",
            "push 0x200",
            "push {:r}",
            "push {:r}",
            "iretq",
            in(reg) GDT.1.user_data.0,
            in(reg) proc.stack_addr,
            in(reg) GDT.1.user_code.0,
            in(reg) proc.alloc_addr + proc.start_addr,
            in("rdi") proc.args_addr,
            in("rsi") proc.args_len as u64,
            options(noreturn)
        );
    }
}

pub fn stack_frame() -> InterruptStackFrameValue {
    let proc = &PROCS.lock()[PROC_COUNTER.load(Ordering::SeqCst)];
    proc.stack_frame
}

pub fn registers() -> Registers {
    let proc = &PROCS.lock()[PROC_COUNTER.load(Ordering::SeqCst)];
    proc.registers
}
