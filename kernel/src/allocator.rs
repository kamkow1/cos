use alloc::alloc::{GlobalAlloc, Layout};
use core::ptr::null_mut;
use x86_64::{
    structures::paging::{
        mapper::MapToError, FrameAllocator, Mapper, Page, PageTableFlags, Size4KiB,
        page::PageRangeInclusive,
    },
    VirtAddr,
};
use crate::memory;
use gen_alloc::fixed_size_block::FixedSizeBlockAllocator;
use gen_alloc::Locked;
use sys_api;

pub const KERNEL_HEAP_START: usize = 0x_4444_4444_0000;
pub const KERNEL_HEAP_SIZE: usize  = 15 << 20; // 15 MB
pub const USER_HEAP_START: usize   = KERNEL_HEAP_START + KERNEL_HEAP_SIZE;
pub const USER_HEAP_SIZE: usize    = 5 << 20; // 5 MB

#[global_allocator]
pub static KERNEL_ALLOCATOR: Locked<FixedSizeBlockAllocator> = Locked::new(FixedSizeBlockAllocator::new());

pub static USER_ALLOCATOR: Locked<FixedSizeBlockAllocator> = Locked::new(FixedSizeBlockAllocator::new());

pub fn init_heap(
    mapper: &mut impl Mapper<Size4KiB>,
    frame_allocator: &mut impl FrameAllocator<Size4KiB>,
) -> Result<(), MapToError<Size4KiB>> {
    // kernel heap
    { 
        let page_range = {
            let heap_start = VirtAddr::new(KERNEL_HEAP_START as u64);
            let heap_end = heap_start + KERNEL_HEAP_SIZE - 1u64;
            let heap_start_page = Page::containing_address(heap_start);
            let heap_end_page = Page::containing_address(heap_end);
            Page::range_inclusive(heap_start_page, heap_end_page)
        };

        for page in page_range {
            let frame = frame_allocator
                .allocate_frame()
                .ok_or(MapToError::FrameAllocationFailed)?;
            let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;
            unsafe { mapper.map_to(page, frame, flags, frame_allocator)?.flush() };
        }

        // kernel allocator
        // can read and write anywhere
        unsafe { KERNEL_ALLOCATOR.lock().init(KERNEL_HEAP_START, KERNEL_HEAP_SIZE) };
    }
    // user heap
    {
        let page_range = {
            let heap_start = VirtAddr::new(USER_HEAP_START as u64);
            let heap_end = heap_start + USER_HEAP_SIZE - 1u64;
            let heap_start_page = Page::containing_address(heap_start);
            let heap_end_page = Page::containing_address(heap_end);
            Page::range_inclusive(heap_start_page, heap_end_page)
        };

        for page in page_range {
            let frame = frame_allocator
                .allocate_frame()
                .ok_or(MapToError::FrameAllocationFailed)?;
            let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE | PageTableFlags::USER_ACCESSIBLE;
            unsafe { mapper.map_to(page, frame, flags, frame_allocator)?.flush() };
        }
        // user allocator
        // restricted to Ring3
        unsafe { USER_ALLOCATOR.lock().init(USER_HEAP_START, USER_HEAP_SIZE) };
    }

    Ok(())
}
