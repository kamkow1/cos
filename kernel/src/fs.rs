use alloc::boxed::Box;
use alloc::vec::Vec;
use alloc::alloc::{alloc, Layout};
use alloc::format;
use core::str;
use crate::ata;
use crate::println;
use crate::logging;
use lazy_static::lazy_static;
use spin::Mutex;
use fsys::superblock::SIGNATURE;
use fsys::FileSystem;
use fsys::KERNEL_SIZE;
use fsys::inode::{INodeKind, INode};
use fsys::FileSystemError;

lazy_static! {
    pub static ref FILESYSTEM: Mutex<Option<Box<FileSystem>>> = Mutex::new(None);
}

#[derive(Debug)]
pub enum FSysSyncError {
    FSysUninitialized,
}

/// Writes the filesystem to the drive
pub fn sync_filesystem(drive: &ata::Drive) -> Result<(), FSysSyncError> {
    let start_addr = KERNEL_SIZE / ata::BLOCK_SIZE;
    let fs_size = FileSystem::size();
    if *logging::LOGS_ENABLED.lock() {
        logging::log(format!("FS: Filesystem structure size = {}", fs_size).as_str());
    } else {
        println!("FS: Filesystem structure size = {}", fs_size);
    }
    // the filesystem has to fit on 512 byte blocks evenly
    assert!(fs_size % 512 == 0); 

    let bytes = FILESYSTEM.lock().as_ref().unwrap().bytes();
    let blocks = fs_size / 512;
    let mut index: usize = 0;
    for i in 0..blocks {
        let old_index = index;
        index += ata::BLOCK_SIZE;
        ata::write_ata(
            drive.bus,
            drive.dsk,
            (start_addr as u32) + (i as u32),
            &bytes[old_index..index],
        ).unwrap();
    }
    Ok(())
}

#[derive(Debug)]
pub enum FSysLoadError {
    ReadError,
    AllocationError,
}

/// Loads the Filesystem into memory
pub fn load_filesystem(
    drive: &ata::Drive,
    options: fsys::Options,
) -> Result<(), FSysLoadError> {
    let start_addr = if options.boot_drive {
        KERNEL_SIZE / ata::BLOCK_SIZE
    } else {
        0usize
    };

    if *logging::LOGS_ENABLED.lock() {
        logging::log(format!("start_addr = {}", start_addr * ata::BLOCK_SIZE).as_str());
    } else {
        println!("start_addr = {}", start_addr * ata::BLOCK_SIZE);
    }

    const FS_SIZE: usize = FileSystem::size();
    // the filesystem has to fit on 512 byte blocks evenly
    assert!(FS_SIZE % 512 == 0); 

    if *logging::LOGS_ENABLED.lock() {
        logging::log(format!("FS: Filesystem structure size = {}, {} blocks", FS_SIZE, FS_SIZE / 512).as_str());
    } else {
        println!("FS: Filesystem structure size = {}, {} blocks", FS_SIZE, FS_SIZE / 512);
    }

    unsafe {
        let mut index: usize = 0;
        let buf_ptr = alloc(Layout::new::<[u8; FS_SIZE]>()) as *mut [u8; FS_SIZE];
        if buf_ptr.is_null() {
            return Err(FSysLoadError::AllocationError);
        }
        let mut buf = Box::from_raw(buf_ptr);
        for i in 0..FS_SIZE / 512 {
            let addr = start_addr as u32 + i as u32;
            let old_index = index;
            index += ata::BLOCK_SIZE;
        
            let mut tmp = [0u8; ata::BLOCK_SIZE];
            if ata::read_ata(drive.bus, drive.dsk, addr, &mut tmp).is_err() {
                return Err(FSysLoadError::ReadError);
            }
            let _ = &buf[old_index..index].clone_from_slice(&tmp);
        }

        *FILESYSTEM.lock() = Some(Box::new(FileSystem::from_bytes(&*buf, &mut 0)));
    }
    Ok(())
}

pub fn check_signature() -> bool {
    FILESYSTEM.lock().as_ref().unwrap().superblock.physical.signature == *SIGNATURE
}

pub fn create(name: &str, kind: INodeKind) -> Result<u32, FileSystemError> {
    let name = fsys::make_path(name);
    FILESYSTEM.lock().as_mut().unwrap().alloc_inode(&name, kind)
}

pub fn open(name: &str) -> Result<u32, FileSystemError> {
    let name = fsys::make_path(name);
    FILESYSTEM.lock().as_ref().unwrap().fd(&name)
}

pub fn write_bytes(fd: u32, bytes: &[u8]) -> Result<(), FileSystemError> {
    FILESYSTEM.lock().as_mut().unwrap().write_bytes(fd, bytes)
}

pub fn write_byte(fd: u32, position: i64, data: u8) -> Result<(), FileSystemError> {
    FILESYSTEM.lock().as_mut().unwrap().write_byte(fd, position, data)
}

pub fn read_bytes(fd: u32) -> Vec<u8> {
    FILESYSTEM.lock().as_ref().unwrap().read_bytes(fd)
}

pub fn read_byte(fd: u32, position: i64) -> u8 {
    FILESYSTEM.lock().as_ref().unwrap().read_byte(fd, position)
}

pub fn remove(name: &str, kind: INodeKind) -> Result<(), FileSystemError> {
    let name = fsys::make_path(name);
    FILESYSTEM.lock().as_mut().unwrap().dealloc_inode(&name, kind)
}

pub fn fd_info(fd: u32) -> INode {
    FILESYSTEM.lock().as_ref().unwrap().inodes[fd as usize]
}

/// Initializes the COS Filesystem for a mounted drive
pub fn init(drive: &ata::Drive, options: fsys::Options) {
    if *logging::LOGS_ENABLED.lock() {
        logging::log("FS: Initializing the filesystem");
    } else {
        println!("FS: Initializing the filesystem");
    }

    load_filesystem(drive, options).unwrap();
    if !check_signature() {
        if *logging::LOGS_ENABLED.lock() {
            logging::log("FS: error - Unformatted drive");
        } else {
            println!("FS: error - Unformatted drive");
        }
        return;
    }

    if *logging::LOGS_ENABLED.lock() {
        logging::log(format!("{:#?}", FILESYSTEM.lock().as_ref().unwrap().superblock).as_str());
    } else {
        println!("{:#?}", FILESYSTEM.lock().as_ref().unwrap().superblock);
    }

    // small check if everything works
    let catalog = "Root-Catalog\\X";
    let name = "Root-Catalog\\X\\Fs-Check.text";
    let check = b"This is a filesystem check";

    println!("FS: Check file path: {}", name);

    // file
    let _fd = create(&catalog, INodeKind::Catalog).unwrap();
    let _fd = create(&name, INodeKind::File).unwrap();

    // get and write
    let fd = open(&name).unwrap();
    write_bytes(fd, check).unwrap();
    sync_filesystem(drive).unwrap();

    // open and read
    let fd = open(&name).unwrap();
    let bytes = read_bytes(fd);
    if *logging::LOGS_ENABLED.lock() {
        logging::log(format!("FS: check file: {}", str::from_utf8(&bytes).unwrap()).as_str());
    } else {
        println!("FS: check file: {}", str::from_utf8(&bytes).unwrap());
    }
    assert_eq!(&bytes, check);

    // delete file
    remove(&catalog, INodeKind::Catalog).unwrap();
    assert!(open(&catalog).is_err());

    let fd = open("Root-Catalog\\fsys-ok.check");
    if fd.is_err() {
        if options.boot_drive {
            if *logging::LOGS_ENABLED.lock() {
                logging::log("FS: setting up the catalog structure");
            } else {
                println!("FS: setting up the catalog structure");
            }
            create("Root-Catalog\\fsys-ok.check", INodeKind::File).unwrap();
            sync_filesystem(drive).unwrap();
        }
    }
}
