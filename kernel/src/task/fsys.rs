use crate::fs;
use crate::ata;
use crate::logging;

pub async fn sync_drive_periodically(
    drive: ata::Drive,
    options: fsys::Options,
) {
    loop {
        logging::log("\nFS: Syncing...\n");
        let mut counter: usize = 1000_000;
        while counter > 0 {
            counter -= 1;
        }
        fs::sync_filesystem(&drive).unwrap();
        fs::load_filesystem(&drive, options).unwrap();
        logging::log("\nFS: Synced!\n");
    }
}
