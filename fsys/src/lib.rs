#![no_std]

extern crate alloc;

pub mod superblock;
pub mod block;
pub mod inode;

use alloc::vec::Vec;
use alloc::string::String;
use core::mem;
use core::str;
use superblock::{SuperBlock, PhysicalSuperBlock};
use inode::{INode, INodeKind};
use block::{Block, BLOCK_SIZE, BLOCK_BYTE_SIZE};
    
pub const KERNEL_SIZE: usize = 2 << 20;

#[derive(Debug, Clone, Copy)]
pub struct Options {
    pub boot_drive: bool,
}

pub const NAME_SIZE: usize = 100;
pub type Name = [u8; NAME_SIZE];

pub fn make_name(s: &str) -> Name {
    assert!(s.len() <= NAME_SIZE, "s = {}", s);

    let mut data = [0u8; NAME_SIZE];
    let _ = &data[..].fill(0);
    let _ = &data[..s.len()].clone_from_slice(s.as_bytes());
    data
}

pub fn name_to_string(name: &Vec<Name>) -> String {
    let mut s = String::new();
    for (i, n) in name.into_iter().enumerate() {
        let x = n.to_vec();
        let x: Vec<u8> = x.into_iter().filter(|c| *c != 0).collect();
        s.push_str(str::from_utf8(&x).unwrap());
        if i < name.len() - 1 {
            s.push_str("\\");
        }
    }
    s
}

pub fn make_path(s: &str) -> Vec<Name> {
    let components = s.split('\\');
    let mut path: Vec<Name> = Vec::new();
    for component in components {
        path.push(make_name(component));
    }
    path
}

pub const INODE_COUNT: usize = 64;
pub const BLOCK_COUNT: usize = 256;

pub const ROOT_CATALOG_NAME: &str = "Root-Catalog";

#[derive(Debug)]
pub enum FileSystemError {
    FileNotFound(String),
    NoFreeBlockFound,
}

#[derive(Debug, Clone, Copy)]
pub struct FileSystem {
    pub superblock: SuperBlock,
    pub inodes: [INode; INODE_COUNT],
    pub blocks: [Block; BLOCK_COUNT],
}

impl FileSystem {
    pub fn new() -> Self {
        let mut filesystem = Self {
            superblock: SuperBlock::new(BLOCK_SIZE as u32, KERNEL_SIZE as u32),
            inodes: [INode::new(INodeKind::File); INODE_COUNT],
            blocks: [Block::new(); BLOCK_COUNT],
        };
        // allocate root catalog
        filesystem.alloc_inode(&make_path(ROOT_CATALOG_NAME), INodeKind::Catalog).unwrap();
        filesystem
    }

    pub fn empty() -> Option<Self> {
        None
    }

    pub fn from_components(superblock: SuperBlock, inodes: [INode; INODE_COUNT], blocks: [Block; BLOCK_COUNT],
    ) -> Self {
        Self { superblock, inodes, blocks,
        }
    }
    
    pub fn alloc_inode(&mut self, name: &Vec<Name>, kind: INodeKind) -> Result<u32, FileSystemError> {
        let inode = INode::find_empty(&self.superblock, &self.inodes);
        let block = Block::find_empty(&self.superblock, &self.blocks);

        match (inode, block) {
            // claim
            (Some((_inode, inode_idx)), Some((_block, block_idx))) => {
                let inode = &mut self.inodes[inode_idx as usize];

                if kind == INodeKind::File {
                    inode.first_block = block_idx as i64;
                    self.blocks[block_idx as usize].next_block = -2;
                }

                inode.name = name[name.len() - 1];
                inode.kind = kind;

                if name.len() > 1 && inode_idx != 0 {
                    let mut name = name.to_vec();
                    name.pop(); // remove the target, keep parents
                    let parent = self.fd(&name)?;
                    let unclaimed = self.inodes[parent as usize].find_unclaimed_child().unwrap();
                    self.inodes[parent as usize].children[unclaimed as usize] = inode_idx as i64;
                }
                Ok(inode_idx)
            },
            _ => Err(FileSystemError::FileNotFound(name_to_string(name))),
        }
    }


    pub fn dealloc_inode(&mut self, name: &Vec<Name>, kind: INodeKind) -> Result<(), FileSystemError> {
        let fd = self.fd(name)?;
        if kind == INodeKind::Catalog {
            let children = self.inodes[fd as usize].children;
            for child_idx in children.into_iter() {
                if child_idx == -1 {
                    continue;
                }
                let mut name = name.to_vec();
                let child = self.inodes[child_idx as usize];
                name.push(child.name);
                self.dealloc_inode(&name, child.kind)?;
            }
        } else {
            self.set_file_size(fd, 1u32)?;
        }
        let mut name = name.to_vec();
        name.pop();
        let parent = self.fd(&name)?;
        let children = self.inodes[parent as usize].children;
        for (i, child) in children.into_iter().enumerate() {
            if child == (fd as i64) {
                self.inodes[parent as usize].children[i] = -1i64;
            }
        }
        self.inodes[fd as usize] = INode::new(INodeKind::File);
        Ok(())
    }

    pub fn fd(&self, name: &Vec<Name>) -> Result<u32, FileSystemError> {
        let root_name = make_name(ROOT_CATALOG_NAME);
        if name.len() == 1 && name[0] == root_name {
            return Ok(0u32);
        }
        let mut current_inode_idx = 0usize;

        'iter_name: for (current_name_idx, name_segment) in name.clone().into_iter().enumerate() {
            let inode = self.inodes[current_inode_idx];
            'iter_children: for child_idx in inode.children.into_iter() {
                if child_idx == -1 {
                    continue 'iter_children;
                }
                let child = self.inodes[child_idx as usize];
                if child.name == name_segment {
                    if current_name_idx == name.len() - 1 {
                        return Ok(child_idx as u32);
                    }
                    current_inode_idx = child_idx as usize;
                    continue 'iter_name;
                }
            }
        }
        Err(FileSystemError::FileNotFound(name_to_string(name)))
    }

    fn shrink_file(&mut self, block: i64) {
        let x = self.blocks[block as usize].next_block;
        if x >= 0 {
            self.shrink_file(x);
        }
        self.blocks[block as usize].next_block = -1;
    }

    pub fn set_file_size(&mut self, fd: u32, size: u32) -> Result<(), FileSystemError> {
        let x = size + (BLOCK_SIZE as u32) - 1;
        let mut x = x / BLOCK_SIZE as u32;

        self.inodes[fd as usize].size = size;
        let mut block = self.inodes[fd as usize].first_block;
        x -= 1;
        // grow
        while x > 0 {
            // check next block no
            let next = self.blocks[block as usize].next_block;
            if next == -2 {
                let empty = match Block::find_empty(&self.superblock, &self.blocks) {
                    Some(e) => Ok(e),
                    None => Err(FileSystemError::NoFreeBlockFound),
                }?;
                self.blocks[block as usize].next_block = empty.1;
                self.blocks[empty.1 as usize].next_block = -2;
            }
            block = self.blocks[block as usize].next_block;
            x -= 1;
        }

        self.shrink_file(block);
        self.blocks[block as usize].next_block = -2;
        Ok(())
    }

    // TODO: implement negative positions
    pub fn get_block(&self, fd: u32, offset: i64) -> i64 { 
        let mut x = offset;
        let mut block = self.inodes[fd as usize].first_block;
        while x > 0 {
            block = self.blocks[block as usize].next_block;
            x -= 1;
        }
        block
    }
    
    pub fn write_byte(&mut self, fd: u32, position: i64, data: u8) -> Result<(), FileSystemError> {
        // set file size so the bytes fit in
        self.set_file_size(fd, position as u32 + 1)?;
        let relative_block = position / (BLOCK_SIZE as i64);
        let block = self.get_block(fd, relative_block);
        let offset = position % (BLOCK_BYTE_SIZE as i64);
        self.blocks[block as usize].bytes[offset as usize] = data;
        Ok(())
    }

    pub fn write_bytes(&mut self, fd: u32, bytes: &[u8]) -> Result<(), FileSystemError> {
        for (i, b) in bytes.iter().enumerate() {
            self.write_byte(fd, i as i64, *b)?;
        }
        Ok(())
    }

    pub fn read_byte(&self, fd: u32, position: i64) -> u8 {
        let relative_block = position as i64 / BLOCK_SIZE as i64;
        let block = self.get_block(fd, relative_block);
        let offset = position % (BLOCK_BYTE_SIZE as i64);
        assert!(block >= 0);
        assert!(offset >= 0);
        self.blocks[block as usize].bytes[offset as usize]
    }

    pub fn read_bytes(&self, fd: u32) -> Vec<u8> {
        let mut buf: Vec<u8> = Vec::new();
        let inode = self.inodes[fd as usize];
        for i in 0..inode.size {
            let b = self.read_byte(fd, i as i64);
            buf.push(b);
        }
        buf
    }
    
    pub const fn size() -> usize {
        mem::size_of::<PhysicalSuperBlock>() +
        mem::size_of::<[INode; INODE_COUNT]>() +
        mem::size_of::<[Block; BLOCK_COUNT]>()
    }

    pub fn bytes(&self) -> Vec<u8> {
        let mut buf: Vec<u8> = Vec::new();

        let mut sb_bytes = self.superblock.physical.bytes();
        buf.append(&mut sb_bytes);
            
        for inode in &self.inodes[..] {
            let mut inode_bytes = (*inode).bytes();
            buf.append(&mut inode_bytes);
        }
        
        for block in &self.blocks[..] {
            let mut block_bytes = (*block).bytes();
            buf.append(&mut block_bytes);
        }

        buf
    }

    pub fn from_bytes(bytes: &[u8], index: &mut usize) -> Self {
        let old_index = *index;
        *index += mem::size_of::<PhysicalSuperBlock>();
        let psb = PhysicalSuperBlock::from_bytes(&bytes[old_index..*index]);
        let sb = SuperBlock::from_physical(KERNEL_SIZE as u32, BLOCK_SIZE as u32, psb);

        let mut inodes = [INode::new(INodeKind::File); INODE_COUNT];
        for i in 0..sb.physical.inode_count {
            let old_index = *index;
            *index += mem::size_of::<INode>();
            let inode = INode::from_bytes(&bytes[old_index..*index]);
            inodes[i as usize] = inode;
        }
        
        let mut blocks = [Block::new(); BLOCK_COUNT];
        for i in 0..sb.physical.block_count {
            let old_index = *index;
            *index += mem::size_of::<Block>();
            let block = Block::from_bytes(&bytes[old_index..*index]);
            blocks[i as usize] = block;
        }

        Self::from_components(sb, inodes, blocks)
    }
}
