use alloc::vec::Vec;
use core::mem;
use core::fmt;
use crate::superblock::SuperBlock;

pub const BLOCK_SIZE: usize = 512;
pub const BLOCK_BYTE_SIZE: usize = BLOCK_SIZE;

// align to a full block
#[repr(align(512))]
#[derive(Clone, Copy, bincode::Encode, bincode::Decode)]
pub struct Block {
    pub next_block: i64,
    pub bytes: [u8; BLOCK_BYTE_SIZE],
}

impl Block {
    pub fn new() -> Self {
        Self {
            next_block: -1,
            bytes: [0u8; BLOCK_BYTE_SIZE],
        }
    }
    
    pub fn find_empty(sb: &SuperBlock, blocks: &[Self]) -> Option<(Self, i64)> {
        for i in 0..sb.physical.block_count as i64 {
            if blocks[i as usize].next_block == -1 {
                return Some((blocks[i as usize], i));
            }
        }
        None
    }

    pub fn bytes(&self) -> Vec<u8> {
        let mut buf = [0u8; mem::size_of::<Self>()];
        let config = bincode::config::standard();
        let _ = bincode::encode_into_slice(self, &mut buf, config).unwrap();
        buf.to_vec()
    }

    pub fn from_bytes(bytes: &[u8]) -> Self {
        let config = bincode::config::standard();
        let (b, _) = bincode::decode_from_slice(&bytes[..], config).unwrap();
        b
    }
}

impl fmt::Debug for Block {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Block")
            .field("next_block", &self.next_block)
            .field("bytes", &"...")
            .finish()
    }
}
