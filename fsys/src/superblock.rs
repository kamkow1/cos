use core::fmt;
use core::str;
use core::mem;
use alloc::vec::Vec;
use crate::{INODE_COUNT, BLOCK_COUNT};

pub const SIGNATURE: &[u8; 8] = b"COS FSYS";

/// Physical Superblock representation.
/// This structure is meant to be serialized/deserialized and stored the disk.
// align to a full block
#[repr(align(512))]
#[derive(Debug, Clone, Copy, bincode::Encode, bincode::Decode)]
pub struct PhysicalSuperBlock {
    pub signature: [u8; 8],
    pub inode_count: u32,
    pub block_count: u32,
}

impl PhysicalSuperBlock {
    pub fn new() -> Self {
        Self {
            signature: *SIGNATURE,
            inode_count: INODE_COUNT as u32,
            block_count: BLOCK_COUNT as u32,
        }
    }

    pub fn bytes(&self) -> Vec<u8> {
        let mut buf = [0u8; mem::size_of::<Self>()];
        let config = bincode::config::standard();
        let _ = bincode::encode_into_slice(self, &mut buf, config).unwrap();
        buf.to_vec()
    }

    pub fn from_bytes(bytes: &[u8]) -> Self {
        let config = bincode::config::standard();
        let (x, _) = bincode::decode_from_slice(&bytes[..], config).unwrap();
        x
    }
}

/// General Superblock representation.
/// Contains meta fields and physical fields.
/// Meta fields are information on the superblock itself, like it's address etc.
/// Physical fields are what is read and written to the disk.
#[derive(Clone, Copy)]
pub struct SuperBlock {
    pub addr: u32,
    pub physical: PhysicalSuperBlock,
}

impl SuperBlock {
    pub fn new(kernel_size: u32, block_size: u32) -> Self {
        Self {
            addr: kernel_size / block_size,
            physical: PhysicalSuperBlock::new(),
        }
    }

    pub fn from_physical(
        kernel_size: u32,
        block_size: u32,
        physical: PhysicalSuperBlock,
    ) -> Self {
        Self {
            addr: kernel_size / block_size,
            physical,
        }
    }
}

impl fmt::Debug for SuperBlock {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("SuperBlock")
            .field("signature", &str::from_utf8(&self.physical.signature[..]).unwrap())
            .finish()
    }
}
