use alloc::vec::Vec;
use core::mem;
use core::fmt;
use core::str;
use crate::{make_name, Name, INODE_COUNT};
use crate::superblock::SuperBlock;

#[derive(Debug, PartialEq, Clone, Copy, bincode::Encode, bincode::Decode)]
pub enum INodeKind {
    File,
    Catalog,
}

const INODE_CHILD_COUNT: usize = INODE_COUNT - 1;

// align to a full block
#[repr(align(512))]
#[derive(Clone, Copy, bincode::Encode, bincode::Decode)]
pub struct INode {
    pub name: Name,
    pub first_block: i64,
    pub size: u32,
    pub children: [i64; INODE_CHILD_COUNT],
    pub kind: INodeKind,
}

impl INode {
    pub fn new(kind: INodeKind) -> Self {
        Self {
            name: make_name("Empty-File"),
            first_block: -1,
            size: 0u32,
            children: [-1i64; INODE_CHILD_COUNT],
            kind,
        }
    }

    pub fn find_empty(sb: &SuperBlock, inodes: &[Self]) -> Option<(Self, u32)> {
        for i in 0..sb.physical.inode_count {
            let inode = inodes[i as usize];
            if inode.name == make_name("Empty-File") {
                return Some((inodes[i as usize], i));
            }
        }
        None
    }

    pub fn find_unclaimed_child(&self) -> Option<usize> {
        for (j, child) in self.children.iter().enumerate() {
            // find first unclaimed child
            if *child == -1 {
                return Some(j);
            }
        }
        None
    }

    pub fn bytes(&self) -> Vec<u8> {
        let mut buf = [0u8; mem::size_of::<Self>()];
        let config = bincode::config::standard();
        let _ = bincode::encode_into_slice(self, &mut buf, config).unwrap();
        buf.to_vec()
    }

    pub fn from_bytes(bytes: &[u8]) -> Self {
        let config = bincode::config::standard();
        let (x, _) = bincode::decode_from_slice(&bytes[..], config).unwrap();
        x
    }
}

impl fmt::Debug for INode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if *&self.name == make_name("Empty-File") {
            f.debug_struct("INode (Empty)").finish()
        } else {
            let children: &Vec<i64> = &self.children
                .into_iter()
                .filter(|x| *x != -1i64)
                .collect();
            f.debug_struct("INode")
                .field("first_block", &self.first_block)
                .field("name", &str::from_utf8(&self.name).unwrap())
                .field("size", &self.size)
                .field("children", &children)
                .field("kind", &self.kind)
                .finish()
        }
    }
}
