use std::env;
use std::fs::File;
use std::io::{self, BufReader, Read, Write, SeekFrom, Seek};
use std::mem;
use std::str;
use std::vec::Vec;
use fsys::{self, FileSystem, inode::INodeKind, inode::INode};
use fsys::block::{BLOCK_SIZE, BLOCK_BYTE_SIZE};
    
fn main() -> Result<(), io::Error> {
    let args: Vec<_> = env::args().collect();
    let index = if args[1] == "B" {
        fsys::KERNEL_SIZE
    } else {
        0
    };
    println!("index = {}", &index);
    match args[2].as_str() {
        "read" => {
            let mut file = File::open(&args[3])?;
            let mut buf = [0u8; FileSystem::size()];
            file.seek(SeekFrom::Start(index as u64))?;
            file.read_exact(&mut buf)?;
            let filesystem = FileSystem::from_bytes(&buf[..], &mut 0);

            let filename = fsys::make_path(&args[4].as_str());

            let fd = filesystem.fd(&filename).unwrap();
            let bytes = filesystem.read_bytes(fd);
            println!("{}", str::from_utf8(&bytes).unwrap()); 
        },
        "remove" => {
            let mut file = File::options()
                .read(true)
                .write(true)
                .open(&args[3])?;
            let mut buf = [0u8; FileSystem::size()];
            file.seek(SeekFrom::Start(index as u64))?;
            file.read_exact(&mut buf)?;
            let mut filesystem = FileSystem::from_bytes(&buf[..], &mut 0);
            
            let kind = match args[4].as_str() {
                "F" => INodeKind::File,
                "C" => INodeKind::Catalog,
                _ => panic!("unknown inode kind"),
            };
            
            let name = fsys::make_path(&args[5]);
            filesystem.dealloc_inode(&name, kind).unwrap();
            
            let _ = file.seek(SeekFrom::Start(index as u64))?;
            let _ = file.write_all(&filesystem.bytes())?;
        },
        "fmt" => {
            let mut file = File::options()
                .write(true)
                .open(&args[3])?;
            let filesystem = FileSystem::new(); 
            let _ = file.seek(SeekFrom::Start(index as u64))?;
            let _ = file.write_all(&filesystem.bytes())?;
        },
        "write" => {
            let mut file = File::options()
                .write(true)
                .read(true)
                .open(&args[3])?;
   
            let mut buf = [0u8; FileSystem::size()];
            file.seek(SeekFrom::Start(index as u64))?;
            file.read_exact(&mut buf)?;
            let mut filesystem = FileSystem::from_bytes(&buf[..], &mut 0);

            let kind = match args[4].as_str() {
                "F" => INodeKind::File,
                "C" => INodeKind::Catalog,
                _ => panic!("unknown inode kind"),
            };
            
            let path = fsys::make_path(&args[5].as_str());

            filesystem.alloc_inode(&path, kind).unwrap();

            if kind == fsys::inode::INodeKind::File {
                let target = File::open(&args[6])?;
                let mut reader = BufReader::new(target);
                let mut buf = Vec::new();
                let _ = reader.read_to_end(&mut buf);
                buf.push(b'\0');


                let fd = filesystem.fd(&path).unwrap();
                filesystem.write_bytes(fd, &buf).unwrap();
                let bytes = filesystem.read_bytes(fd);
                assert_eq!(&bytes, &buf);
            }

            let _ = file.seek(SeekFrom::Start(index as u64))?;
            let _ = file.write_all(&filesystem.bytes())?;
        },
        "list-inodes" => {
            let mut file = File::open(&args[3])?;
            let mut buf = [0u8; FileSystem::size()];
            file.seek(SeekFrom::Start(index as u64))?;
            file.read_exact(&mut buf)?;
            let filesystem = FileSystem::from_bytes(&buf[..], &mut 0);

            println!("{:#?}", filesystem.inodes);
        },
        "tree" => {
            let mut file = File::open(&args[3])?;
            let mut buf = [0u8; FileSystem::size()];
            file.seek(SeekFrom::Start(index as u64))?;
            file.read_exact(&mut buf)?;
            let filesystem = FileSystem::from_bytes(&buf[..], &mut 0);
            
            let path = fsys::make_path(&args[4].as_str());
            let fd = filesystem.fd(&path).unwrap();

            let inode = filesystem.inodes[fd as usize];

            fn helper(filesystem: &FileSystem, inode: INode, level: i32) {
                for _ in 0..level*4 {
                    print!(" ");
                }
                print!("+ ");

                println!("{}", str::from_utf8(&inode.name).unwrap());
                if inode.kind == INodeKind::Catalog {
                    for idx in inode.children {
                        if idx == -1 {
                            continue;
                        }

                        helper(filesystem, filesystem.inodes[idx as usize], level + 1);
                    }
                }
            }
            helper(&filesystem, inode, 0i32);
        },
        "structure" => {
            let mut file = File::options()
                .write(true)
                .read(true)
                .open(&args[3])?;
            let mut buf = [0u8; FileSystem::size()];
            file.seek(SeekFrom::Start(index as u64))?;
            file.read_exact(&mut buf)?;
            let mut filesystem = FileSystem::from_bytes(&buf[..], &mut 0);

            filesystem.alloc_inode(&fsys::make_path("Root-Catalog\\System"), INodeKind::Catalog).unwrap();
            filesystem.alloc_inode(&fsys::make_path("Root-Catalog\\System\\startup.text"), INodeKind::File).unwrap();
            let startup_file = filesystem.fd(&fsys::make_path("Root-Catalog\\System\\startup.text")).unwrap();
            filesystem.write_bytes(startup_file, b"Root-Catalog\\Programs\\shell.app").unwrap();
            
            filesystem.alloc_inode(&fsys::make_path("Root-Catalog\\System\\Logs"), INodeKind::Catalog).unwrap();
            filesystem.alloc_inode(&fsys::make_path("Root-Catalog\\System\\Logs\\logs.text"), INodeKind::File).unwrap();
            
            filesystem.alloc_inode(&fsys::make_path("Root-Catalog\\System\\Process"), INodeKind::Catalog).unwrap();
            
            filesystem.alloc_inode(&fsys::make_path("Root-Catalog\\Users"), INodeKind::Catalog).unwrap();
            
            filesystem.alloc_inode(&fsys::make_path("Root-Catalog\\Trash"), INodeKind::Catalog).unwrap();
            
            filesystem.alloc_inode(&fsys::make_path("Root-Catalog\\Programs"), INodeKind::Catalog).unwrap();
            
            let _ = file.seek(SeekFrom::Start(index as u64))?;
            let _ = file.write_all(&filesystem.bytes())?;
        },
        _ => {},
    }

    Ok(())
}
