use core::panic::PanicInfo;
use crate::{syscall, syscall::*};

#[cfg_attr(feature = "user_panic", panic_handler)]
pub fn panic(info: &PanicInfo) -> ! {
    let ptr = info as *const PanicInfo;
    unsafe { syscall!(PANIC_NOTIFY, ptr) };

    loop {}
}
