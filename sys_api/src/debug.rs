// Useful utilities for debugging
// These functions cannot depend on the alloc crate (how would you debug heap issues then?)

use crate::{syscall, syscall::*};

fn print_byte(b: u8) {
    let codes = [b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', b'a', b'b', b'c', b'd', b'e', b'f'];
    let nible = ((b >> 4) & 0xF) as usize;
    unsafe { syscall!(VGA_PRINT, codes[nible]) };
    let nible = (b & 0xF) as usize;
    unsafe { syscall!(VGA_PRINT, codes[nible]) };
}

pub fn print_address(ptr: u64) {
    let bytes = ptr.to_ne_bytes();
    /*
    for b in bytes.into_iter().rev() {
        print_byte(b);
    }
    */
    let mut i = bytes.len();
    while i > 0 {
        print_byte(bytes[i - 1]);
        i -= 1;
    }
    unsafe { syscall!(VGA_PRINT, '\n') };
}
