use crate::{syscall, syscall::*};

pub fn out_handle() -> u32 {
    (unsafe { syscall!(OUT_HANDLE) }) as u32
}

pub fn in_handle() -> u32 {
    (unsafe { syscall!(IN_HANDLE) }) as u32
}

pub fn exit() -> u32 {
    (unsafe { syscall!(EXIT) }) as u32
}
