use alloc::string::String;

extern {
    fn main(options: &String);
}

#[cfg(feature = "start_fn")]
#[no_mangle]
pub extern "C" fn _start(argv: u64, argc: usize) {
    let buf = argv as *mut u8;
    let options = unsafe { String::from_raw_parts(buf, argc, argc) };
    unsafe { main(&options) };
}
