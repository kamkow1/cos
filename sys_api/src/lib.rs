#![no_std]

extern crate alloc;
pub extern crate gen_alloc;
pub extern crate fsys;

pub mod syscall;
pub mod fsys2;
pub mod proc;
pub mod allocator;
pub mod debug;
pub mod panicking;
pub mod vga;
pub mod start;
