// safe wrappers around filesystem-related syscalls
// and other utility functions for convenience

use alloc::vec::Vec;
use alloc::boxed::Box;
use fsys::inode::INode;
use alloc::alloc::{Layout, alloc};
use crate::{syscall, syscall::*};

pub fn write_byte(fd: u32, position: usize, data: u8) -> usize {
    unsafe { syscall!(WRITE, fd, position, data) };
    position
}

pub fn write_bytes(fd: u32, data: &[u8]) -> usize {
    let mut position = 0usize;
    for (i, b) in data.into_iter().enumerate() {
        position = write_byte(fd, i, *b);
    }
    position
}

pub fn read_byte(fd: u32, position: usize) -> u8 {
    (unsafe { syscall!(READ, fd, position) }) as u8
}

pub fn read_bytes(fd: u32) -> Vec<u8> {
    let layout = Layout::new::<INode>();
    let ptr = unsafe { alloc(layout) as *mut INode };
    unsafe { syscall!(FD_INFO, fd, ptr) };
    let inode = unsafe { Box::from_raw(ptr) };

    let mut v = Vec::with_capacity(inode.size as usize);

    for i in 0..inode.size {
        let byte = read_byte(fd, i as usize);
        v.push(byte);
    }
    v
}

pub fn append_byte(fd: u32, data: u8) -> usize {
    unsafe { syscall!(APPEND, fd, data) }
}

pub fn append_bytes(fd: u32, data: &[u8]) -> usize {
    let mut res = 0usize;
    for b in data {
        res = append_byte(fd, *b);
    }
    res
}

pub fn open(s: &str) -> Result<usize, ()> {
    let fd = unsafe { syscall!(OPEN, s.as_bytes().as_ptr(), s.len()) };
    if fd == usize::MAX {
        Err(())
    } else {
        Ok(fd)
    }
}
