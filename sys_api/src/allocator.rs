use lazy_static::lazy_static;
use alloc::alloc::{GlobalAlloc, Layout};
use crate::{syscall, syscall::*};

#[cfg_attr(feature = "user_allocator", global_allocator)]
static ALLOCATOR: ApiAllocator = ApiAllocator::new();

struct ApiAllocator;

impl ApiAllocator {
    const fn new() -> Self {
        Self {}
    }
}

unsafe impl GlobalAlloc for ApiAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let ptr = syscall!(MEM_ALLOC, &layout as *const Layout);
        ptr as *mut u8
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        syscall!(MEM_DEALLOC, ptr, &layout as *const Layout);
    }
}
