use core::fmt;
use crate::{syscall, syscall::*};

#[macro_export]
macro_rules! vga_print {
    ($($arg: tt)*) => ($crate::vga::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! vga_println {
    ($($arg: tt)*) => ($crate::vga_print!("{}\n", format_args!($($arg)*)));
}

pub fn _print(args: fmt::Arguments) {
    if let Some(string) = args.as_str() {
        for c in string.chars() {
            unsafe { syscall!(VGA_PRINT, c) };
        }
    }
}
