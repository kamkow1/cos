// syscall numbers
// consts are easier to operate on than enums
pub const EXIT: usize         = 1;
pub const HALT: usize         = 2;
pub const VGA_PRINT: usize    = 3;
pub const WRITE: usize        = 4;
pub const OUT_HANDLE: usize   = 5;
pub const IN_HANDLE: usize    = 6;
pub const READ: usize         = 7;
pub const MEM_ALLOC: usize    = 9;
pub const MEM_DEALLOC: usize  = 10;
pub const PANIC_NOTIFY: usize = 11;
pub const FD_INFO: usize      = 12;
pub const OPEN: usize         = 13;
pub const APPEND: usize       = 14;

pub unsafe fn syscall0(n: usize) -> usize {
    let res: usize;
    core::arch::asm!(
        "int 0x80",
        in("rax") n,
        lateout("rax") res,
    );
    res
}

pub unsafe fn syscall1(n: usize, arg1: usize) -> usize {
    let res: usize;
    core::arch::asm!(
        "int 0x80",
        in("rax") n,
        in("rdi") arg1,
        lateout("rax") res,
    );
    res
}

pub unsafe fn syscall2(n: usize, arg1: usize, arg2: usize) -> usize {
    let res: usize;
    core::arch::asm!(
        "int 0x80",
        in("rax") n,
        in("rdi") arg1,
        in("rsi") arg2,
        lateout("rax") res,
    );
    res
}

pub unsafe fn syscall3(n: usize, arg1: usize, arg2: usize, arg3: usize) -> usize {
    let res: usize;
    core::arch::asm!(
        "int 0x80",
        in("rax") n,
        in("rdi") arg1,
        in("rsi") arg2,
        in("rdx") arg3,
        lateout("rax") res,
    );
    res
}

pub unsafe fn syscall4(n: usize, arg1: usize, arg2: usize, arg3: usize, arg4: usize) -> usize {
    let res: usize;
    core::arch::asm!(
        "int 0x80",
        in("rax") n,
        in("rdi") arg1,
        in("rsi") arg2,
        in("rdx") arg3,
        in("r8")  arg4,
        lateout("rax") res,
    );
    res
}

#[macro_export]
macro_rules! syscall {
    ($n: expr) => (
        $crate::syscall::syscall0($n as usize)
    );
    ($n: expr, $a1: expr) => (
        $crate::syscall::syscall1($n as usize, $a1 as usize)
    );
    ($n: expr, $a1: expr, $a2: expr) => (
        $crate::syscall::syscall2($n as usize, $a1 as usize, $a2 as usize)
    );
    ($n: expr, $a1: expr, $a2: expr, $a3: expr) => (
        $crate::syscall::syscall3($n as usize, $a1 as usize, $a2 as usize, $a3 as usize)
    );
    ($n: expr, $a1: expr, $a2: expr, $a3: expr, $a4: expr) => (
        $crate::syscall::syscall4($n as usize, $a1 as usize, $a2 as usize, $a3 as usize, $a4 as usize)
    );
}
